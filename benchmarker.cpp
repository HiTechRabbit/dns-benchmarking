#include "benchmarker.h"

Benchmarker::Benchmarker(QList<OpennicParser::DnsServer> dnsList, QList<QString> domainsList, QObject *parent) : QDnsLookup(parent){
    mDnsList = dnsList;
    mDomainsList = domainsList;

    this->setType(QDnsLookup::ANY);
    connect(this, &Benchmarker::finished, &this->mLoop, &QEventLoop::quit);
}

void Benchmarker::run() {
    foreach (OpennicParser::DnsServer dns, mDnsList) {
        Score currentScore = Score();
        currentScore.dns = dns;

        this->setNameserver(dns.ipv4);
        currentScore.totalTime.start();

        foreach (QString domain, mDomainsList) {
            this->setName(domain);
            this->lookup();
            this->mLoop.exec();

            if (this->error()) {
                currentScore.errorEncountered = true;
                break;
            }
        }

        currentScore.totalTime = currentScore.totalTime.addSecs(currentScore.totalTime.elapsed());
        mScoresList += currentScore;
    }
}

QList<Benchmarker::Score> Benchmarker::getTopPerformingDnsServers(qint32 numberOfTop) {
    QList<Score> validScoresList;
    foreach(Score score, mScoresList) {
        if (! score.errorEncountered)
            validScoresList.append(score);
    };

    struct {
         bool operator()(Score a, Score b) const {
            return a.totalTime.msec() < b.totalTime.msec();
        }
    } customLess;

    std::sort(validScoresList.begin(), validScoresList.end(), customLess);

    while(validScoresList.count() > numberOfTop)
        validScoresList.takeLast();

    return validScoresList;
}

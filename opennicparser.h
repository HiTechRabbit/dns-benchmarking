#ifndef DNSLISTRETRIEVER_H
#define DNSLISTRETRIEVER_H

#include <QObject>
#include <QList>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QHostAddress>
#include <QEventLoop>

#include <gumbo.h>
#include "string.h"
#include <string>

class OpennicParser : public QObject {
    Q_OBJECT
public:
    struct DnsServer {
        DnsServer(const QString address) {
            QHostAddress temporaryAddress(address);
            if (temporaryAddress.protocol() == QAbstractSocket::NetworkLayerProtocol::IPv4Protocol)
                ipv4 = temporaryAddress;
            else if (temporaryAddress.protocol() == QAbstractSocket::NetworkLayerProtocol::IPv6Protocol)
                ipv6 = temporaryAddress;
        }

        // keep the default constructor so that I can use the simple {} to initialize it
        DnsServer(){}

        QHostAddress ipv4;
        QHostAddress ipv6;
        bool offersEncryption   = false;
        bool areLogsKept        = true;
        bool areLogsAnonymized  = false;
    };

    explicit OpennicParser(QList<DnsServer> *dnsList, bool onlyIpv4, QObject *parent = nullptr);
    bool getDnsServersList();
    QNetworkReply::NetworkError error() { return mEncounteredError; }

private:
    QNetworkAccessManager mDownloader;
    QList<DnsServer> *mpDnsList;
    const QList<QUrl> M_RESOURCES_TO_PARSE_LIST = {
        {QUrl("https://servers.opennic.org/?tier=1&show=PASS")},
        {QUrl("https://servers.opennic.org/?tier=2&show=PASS")}
    };

    const QString CLASS_NAME_IPV4           = "mono ipv4";
    const QString CLASS_NAME_IPV6           = "mono ipv6";
    const QString CLASS_NAME_ENCRYPTED      = "fas fa-lock";
    const QString CLASS_NAME_NO_LOGS        = "fas fa-user-times";
    const QString CLASS_NAME_ANONYMIZED     = "fas fa-user-slash";

    DnsServer mCurrentDnsServer;

    QNetworkReply::NetworkError mEncounteredError;
    QEventLoop mLoop;
    bool mOnlyIpv4;

    void searchForIps(GumboNode* node, const QByteArray originalHtmlData);
    static QString extractText(GumboNode* node);
    void readData(QNetworkReply* pReply);
};

#endif // DNSLISTRETRIEVER_H

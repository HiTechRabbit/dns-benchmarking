#ifndef RESOLVER_H
#define RESOLVER_H

#include <QDnsLookup>
#include <QEventLoop>
#include <QHostAddress>
#include <QObject>
#include <QTime>

#include "opennicparser.h"

class Benchmarker : public QDnsLookup{
    Q_OBJECT
public:
    struct Score{
        OpennicParser::DnsServer dns;
        QTime totalTime;
        bool errorEncountered = false;
    };

    explicit Benchmarker(const QList<OpennicParser::DnsServer> dnsList, const QList<QString> domainsList, QObject *parent = nullptr);
    void run();
    QList<Score> getTopPerformingDnsServers(qint32 numberOfTop = 4);

    QList<Score> getScores() { return mScoresList; }
signals:

private:
    QList<OpennicParser::DnsServer> mDnsList;
    QList<QString> mDomainsList;
    QList<Score> mScoresList;

    QEventLoop mLoop;
};

#endif // RESOLVER_H

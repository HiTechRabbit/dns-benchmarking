#ifndef MAIN_H
#define MAIN_H

#include <QCoreApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QFile>
#include <QFileInfo>
#include <QHostAddress>
#include <QList>
#include <QRegularExpression>
#include <QStringLiteral>
#include <QTextStream>

#include <algorithm>

#include "benchmarker.h"
#include "opennicparser.h"

const QStringList DEFAULT_DNS_SERVERS_IPV4 = {
    "8.8.8.8",
    "8.8.4.4",
    "1.1.1.1",
    "1.0.0.1",
    "208.67.222.222",
    "208.67.220.220",
    "8.26.56.26",
    "8.20.247.20"
};

const QStringList DEFAULT_DNS_SERVERS_IPV6 = {
    "2001:4860:4860::8888",
    "2001:4860:4860::8844",
};

const QStringList DEFAULT_DOMAINS = {
    "duckduckgo.com",
    "yahoo.com",
    "microsoft.com",
    "youtube.com",
    "stackoverflow.com",
    "economist.com",
    "framagit.org",
    "bandcamp.com",
    "cdstm.ch",
    "admin.ch",
    "un.org",
    "wikipedia.org",
    "8ch.net"
};

const qint8 SPACE_BETWEEN_BORDERS = 1;

enum CommandLineResult {
    CommandLineOk = 0,
    CommandLineError,
    CommandLineParsingError,
    CommandLineArgumentError,
};

struct CommandLineArguments {
    QString dnsServersFilePath;
    QString domainsFilePath;
    bool noDefault;
    bool onlyIpv4;
    bool verbose;
    qint32 numberOfResultsToShow = 4;
};

const int NO_WIDTH = 0;
const int RANKING_WIDTH = 3;
const int SUPPLEMENTARY_BORDER_WIDTH = 2;

int main(int argc, char *argv[]);
bool parseArguments(QCoreApplication *coreApplication, CommandLineArguments *arguments);
template<typename T>inline bool parseFile(QString filePath, QList<T> *list, QRegularExpression regularExpression);
void showTopPerformingDnsServers(QList<Benchmarker::Score> *scoresList);

#endif // MAIN_H

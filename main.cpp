#include "main.h"

int main(int argc, char *argv[]) {
    QCoreApplication coreApplication(argc, argv);
    QCoreApplication::setApplicationName("dns-benchmark");
    QCoreApplication::setApplicationVersion("0.0.1");

    CommandLineArguments arguments;
    bool isParsingSuccessfull = parseArguments(&coreApplication, &arguments);

    if (! isParsingSuccessfull) {
        QTextStream(stderr) << "Argument error.\n";
        return CommandLineArgumentError;
    }

    QList<OpennicParser::DnsServer> dnsList;
    QList<QString> domainsList;

    if (! arguments.noDefault) {
        OpennicParser retriever(&dnsList, arguments.onlyIpv4, qApp);

        bool isRetrievingUrlSuccessfull = retriever.getDnsServersList();

        if (! isRetrievingUrlSuccessfull) {
            QTextStream(stderr) << QString("Error while retrieving DNS servers online: %1 \n").arg(retriever.error());
            return CommandLineError;
        }

        QStringList defaultDnsServers = DEFAULT_DNS_SERVERS_IPV4;
        if (! arguments.onlyIpv4)
            defaultDnsServers = defaultDnsServers + DEFAULT_DNS_SERVERS_IPV6;

        foreach (QString dnsServer, defaultDnsServers) {
            OpennicParser::DnsServer completeDnsServer(dnsServer);
            dnsList.append(completeDnsServer);
        }

        foreach (QString domain, DEFAULT_DOMAINS) {
            domainsList.append(domain);
        }
    }

    if (! arguments.domainsFilePath.isNull()) {
        QRegularExpression validHostNameRegex("^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9]))*$");
        bool isDomainsFileParsingSuccessfull = parseFile<QString>(arguments.domainsFilePath, &domainsList, validHostNameRegex);

        if (! isDomainsFileParsingSuccessfull) {
            QTextStream(stderr) << QString("Error while reading %1.\n").arg(arguments.domainsFilePath);
            return CommandLineParsingError;
        }
    }

    if (! arguments.dnsServersFilePath.isNull()) {
        QRegularExpression validIpAddressRegex("((^\\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\\s*$)|(^\\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:)))(%.+)?\\s*$))");
        bool isDomainsFileParsingSuccessfull = parseFile<OpennicParser::DnsServer>(arguments.dnsServersFilePath, &dnsList, validIpAddressRegex);

        if (! isDomainsFileParsingSuccessfull) {
            QTextStream(stderr) << QString("Error while reading %1.\n").arg(arguments.domainsFilePath);
            return CommandLineParsingError;
        }
    }

    QTextStream(stdout) << QString("%1 domains to resolve have been found.\n").arg(domainsList.size());
    QTextStream(stdout) << QString("%1 dns servers have been found.\n").arg(dnsList.size());

    Benchmarker benchmarker(dnsList, domainsList, &coreApplication);
    if (arguments.verbose)
        coreApplication.connect(&benchmarker, &Benchmarker::finished,[&benchmarker]() {
            if (benchmarker.error())
                QTextStream(stderr) << QStringLiteral("%1: %2 while resolving %3\n").arg(benchmarker.nameserver().toString()).arg(benchmarker.errorString()).arg(benchmarker.name());
        });

    benchmarker.run();
    QList<Benchmarker::Score> scoresList = benchmarker.getTopPerformingDnsServers(arguments.numberOfResultsToShow);

    showTopPerformingDnsServers(&scoresList);

    return CommandLineOk;
}

bool parseArguments(QCoreApplication *coreApplication, CommandLineArguments *arguments) {
    QCommandLineParser parser;
    parser.setApplicationDescription("Benchmarks a bunch of different DNS servers.");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption domainsFilePathOption("domains-file", "Source file containing the domains to resolve.", "Filepath");
    parser.addOption(domainsFilePathOption);

    QCommandLineOption dnsServersFilePathOption("dns-servers-file", "Source file containing the resolvers.", "Filepath");
    parser.addOption(dnsServersFilePathOption);

    QCommandLineOption topResultsOption("t", "Show the top t dns servers", "Number of results to show");
    parser.addOption(topResultsOption);

    QCommandLineOption noDefaultOption(QStringList() << "n" << "no-default", "Disable the default DNS servers and domains.", "IP Address of a DNS server");
    parser.addOption(noDefaultOption);

    QCommandLineOption onlyIpv4Option("only-ipv4", "Do not try to resolve with ipv6.");
    parser.addOption(onlyIpv4Option);

    QCommandLineOption verboseOption("verbose", "Gonna show more info on whats going on.");
    parser.addOption(verboseOption);

    parser.process(*coreApplication);

    arguments->domainsFilePath = parser.value(domainsFilePathOption);
    if (! QFileInfo(arguments->domainsFilePath).exists() && ! arguments->domainsFilePath.isNull())
        return false;

    arguments->dnsServersFilePath = parser.value(dnsServersFilePathOption);
    if (! QFileInfo(arguments->dnsServersFilePath).exists() && ! arguments->dnsServersFilePath.isNull())
        return false;

    arguments->noDefault = parser.isSet(noDefaultOption);
    arguments->onlyIpv4 = parser.isSet(onlyIpv4Option);
    arguments->verbose = parser.isSet(verboseOption);

    if (parser.isSet(topResultsOption)) {
        bool conversionSuccessful;
        arguments->numberOfResultsToShow  = parser.value(topResultsOption).toInt(&conversionSuccessful);
    }

    return true;
}

template<typename T> inline bool parseFile(QString filePath, QList<T> *list, QRegularExpression regularExpression) {
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
       return false;

    QTextStream fileStream(&file);
    while (!fileStream.atEnd()) {
        QString data = fileStream.readLine();
        T line(data);
        bool isValid = regularExpression.match(data).hasMatch();

        if (isValid)
            list->append(line);
    }

    file.close();

    return !fileStream.status();
}

void showTopPerformingDnsServers(QList<Benchmarker::Score> *scoresList) {
    // minimum values that corresponds to the length of strings "ipv4 address", "ipv6 address" and "timems"
    // necessary to format correctly when scores and times are smaller that those titles
    qint32 widthIpv4 = 12, widthIpv6 = 12, widthTotalTime = 6;
    const qint32 widthLogsKept = 8, widthAnonymized = 10, widthEncryption = 10;

    foreach(Benchmarker::Score score, *scoresList) {
        int currentWidthIpv4 = score.dns.ipv4.toString().length();
        int currentWidthIpv6 = score.dns.ipv6.toString().length();
        int currentWidthTotalTime = QStringLiteral("%1").arg(score.totalTime.msec()).length();
        if (currentWidthIpv4 > widthIpv4)
            widthIpv4 = currentWidthIpv4;
        if (currentWidthTotalTime > widthTotalTime)
            widthTotalTime = currentWidthTotalTime;
        if (currentWidthIpv6 > widthIpv6)
            widthIpv6 = currentWidthIpv6;
    }

    QTextStream out(stdout);
    out.setFieldAlignment(QTextStream::AlignCenter);
    out << endl << "Results:" << endl;
    out << qSetFieldWidth(RANKING_WIDTH)    << "#"                          << qSetFieldWidth(NO_WIDTH)                     << "|"
        << qSetFieldWidth(widthIpv4         + SUPPLEMENTARY_BORDER_WIDTH)   << "ipv4 address" << qSetFieldWidth(NO_WIDTH)   << "|"
        << qSetFieldWidth(widthIpv6         + SUPPLEMENTARY_BORDER_WIDTH)   << "ipv6 address" << qSetFieldWidth(NO_WIDTH)   << "|"
        << qSetFieldWidth(widthLogsKept     + SUPPLEMENTARY_BORDER_WIDTH)   << "logs Kept"    << qSetFieldWidth(NO_WIDTH)   << "|"
        << qSetFieldWidth(widthAnonymized   + SUPPLEMENTARY_BORDER_WIDTH)   << "anonymized"   << qSetFieldWidth(NO_WIDTH)   << "|"
        << qSetFieldWidth(widthEncryption   + SUPPLEMENTARY_BORDER_WIDTH)   << "encryption"   << qSetFieldWidth(NO_WIDTH)   << "|"
        << qSetFieldWidth(widthTotalTime    + SUPPLEMENTARY_BORDER_WIDTH)   << "time"         << qSetFieldWidth(NO_WIDTH)   << endl;

    for(qint8 i = 0; i < scoresList->size(); i++){
        Benchmarker::Score score = scoresList->at(i);
        QChar logsKept = ' ', anonymized = ' ', encryption = ' ';
        if(score.dns.areLogsKept)       logsKept   = 'X';
        if(score.dns.areLogsAnonymized) anonymized = 'X';
        if(score.dns.offersEncryption)  encryption = 'X';

        out << qSetFieldWidth(RANKING_WIDTH)    << i + 1                        << qSetFieldWidth(NO_WIDTH)     << "|"
            << qSetFieldWidth(widthIpv4         + SUPPLEMENTARY_BORDER_WIDTH)   << score.dns.ipv4.toString()    << qSetFieldWidth(NO_WIDTH)     << "|"
            << qSetFieldWidth(widthIpv6         + SUPPLEMENTARY_BORDER_WIDTH)   << score.dns.ipv6.toString()    << qSetFieldWidth(NO_WIDTH)     << "|"
            << qSetFieldWidth(widthLogsKept     + SUPPLEMENTARY_BORDER_WIDTH)   << logsKept                     << qSetFieldWidth(NO_WIDTH)     << "|"
            << qSetFieldWidth(widthAnonymized   + SUPPLEMENTARY_BORDER_WIDTH)   << anonymized                   << qSetFieldWidth(NO_WIDTH)     << "|"
            << qSetFieldWidth(widthEncryption   + SUPPLEMENTARY_BORDER_WIDTH)   << encryption                   << qSetFieldWidth(NO_WIDTH)     << "|"
            << qSetFieldWidth(widthTotalTime    + SUPPLEMENTARY_BORDER_WIDTH)   << QStringLiteral("%1ms").arg(score.totalTime.msec()) << qSetFieldWidth(NO_WIDTH) << endl;
    }
}

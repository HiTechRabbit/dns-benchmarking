    #include "opennicparser.h"

OpennicParser::OpennicParser(QList<DnsServer> *dnsList, bool onlyIpv4, QObject *parent): QObject(parent) {
    mpDnsList = dnsList;
    mOnlyIpv4 = onlyIpv4;
    mCurrentDnsServer = {};
}

bool OpennicParser::getDnsServersList() {
    foreach(QUrl ressource, M_RESOURCES_TO_PARSE_LIST) {
        QNetworkReply *pReply = mDownloader.get(QNetworkRequest(ressource));
        connect(&mDownloader, &QNetworkAccessManager::finished, &mLoop, &QEventLoop::quit);
        mLoop.exec();

        if (pReply->error()){
            mEncounteredError = pReply->error();
            return false;
        }

        readData(pReply);
    }

    return true;
}

void OpennicParser::readData(QNetworkReply* pReply) {
    const QByteArray data = pReply->readAll();

    GumboOutput* htmlOutput = gumbo_parse(data);
    searchForIps(htmlOutput->root, data);
}

void OpennicParser::searchForIps(GumboNode* node, const QByteArray originalHtmlData) {
    if (node->type != GUMBO_NODE_ELEMENT)
        return;

    if (node->v.element.tag == GUMBO_TAG_P) {
        if (! mCurrentDnsServer.ipv4.isNull())
            mpDnsList->append(mCurrentDnsServer);
        mCurrentDnsServer = DnsServer{};
    }

    //QString classAttribute(gumbo_get_attribute(&node->v.element.attributes, "class")->value);
    GumboAttribute* gumboAttribute = gumbo_get_attribute(&node->v.element.attributes, "class");
    if (gumboAttribute != nullptr) {

        QString classAttribute(gumboAttribute->value);
        QString nodeText = extractText(node);

        if (! classAttribute.compare(CLASS_NAME_IPV4)) {
            QHostAddress address(nodeText);

            if (! address.isNull())
                mCurrentDnsServer.ipv4 = address;
        } else if (! classAttribute.compare(CLASS_NAME_IPV6)) {
            QHostAddress address(nodeText);

            if (! address.isNull())
                mCurrentDnsServer.ipv6 = address;

        } else if (! classAttribute.compare(CLASS_NAME_NO_LOGS))
            mCurrentDnsServer.areLogsKept = false;

        else if (! classAttribute.compare(CLASS_NAME_ANONYMIZED))
            mCurrentDnsServer.areLogsAnonymized = true;

        else if (! classAttribute.compare(CLASS_NAME_ENCRYPTED))
            mCurrentDnsServer.offersEncryption = true;
    }

    GumboVector* children = &node->v.element.children;
    for (unsigned int i = 0; i < children->length; ++i)
        searchForIps(static_cast<GumboNode*>(children->data[i]), originalHtmlData);
}

/**
 * @brief Extrait la chaîne de charactère d'un node xhtml.
 * @param node Node dont on veut extraire le contenu.
 * @return Chaîne entière ou bout de chaîne.
 */
QString OpennicParser::extractText(GumboNode* node) {
    if (node->type == GUMBO_NODE_TEXT)
        return QString(node->v.text.text);
    else if (node->type == GUMBO_NODE_ELEMENT &&
               node->v.element.tag != GUMBO_TAG_SCRIPT &&
               node->v.element.tag != GUMBO_TAG_STYLE) {

        QString contents = "";
        GumboVector* children = &node->v.element.children;

        for (unsigned int i = 0; i < children->length; ++i) {
            const QString text = extractText((GumboNode*) children->data[i]);

            contents.append(text);
        }
        return contents;
    } else {
        return "";
    }
}
